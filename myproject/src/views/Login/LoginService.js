const LoginService = {
  LoginList: [
    { id: 1, username: 'Nutthawut', password: '12345678' },
    { id: 2, username: 'Siriyakorn', password: '87654321' }
  ],
  listId: 3,
  addLogin (login) {
    login.id = this.listId++
    this.LoginList.push(login)
  },
  updateLogin (login) {
    const index = this.LoginList.findIndex(item => item.id === login.id)
    this.LoginList.splice(index, 1, login)
  },
  deleteLogin (login) {
    const index = this.LoginList.findIndex(item => item.id === login.id)
    this.LoginList.splice(index, 1)
  },
  getLogin () {
    return [...this.LoginList]
  }
}
export default LoginService
